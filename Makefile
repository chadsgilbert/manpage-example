
PREFIX=~/.local

.PHONY: all doc install
all: doc

doc: share/man/man1/example.1 share/man/man3/example.3

share/man/man1/example.1: example.1.adoc
	asciidoctor -b manpage example.1.adoc -o share/man/man1/example.1

share/man/man3/example.3: example.3.adoc
	asciidoctor -b manpage example.3.adoc -o share/man/man3/example.3

install: doc
	mkdir -p $(PREFIX)/share && cp -r share/man $(PREFIX)/share

